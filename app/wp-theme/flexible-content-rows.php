<?php
$global_count = 0;
if(have_rows('flexible_content')) {
    while (have_rows('flexible_content')) {
        the_row();

        $row_layout = get_row_layout();
        $layout_path = __DIR__."/page-templates/flexible-contents/{$row_layout}.php";

        if(file_exists($layout_path)) {

            include $layout_path;
        }
    }
}
