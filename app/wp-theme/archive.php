<?php
/**
 * The template for displaying Archive pages.
 */

get_header(); ?>
 
<?php 

   	global $paged;

	if( get_query_var( 'paged' ) )
		$my_page = get_query_var( 'paged' );
	else {
		if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		else
			$my_page = 1;
		set_query_var( 'paged', $my_page );
		$paged = $my_page;
	}
	$args = array(
        'post_type' => 'post', 
        'orderby' => 'date', 
        'order'   => 'DESC', 
        'posts_per_page' => 5,
        'category_name' => single_cat_title('',false),
        'paged'=> $my_page
    );
	$query = new WP_Query( $args );
 ?>

		<section class="archive">
		
			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					
				<?php endwhile; ?>
				<?php else : ?>

					

			<?php endif; 
				wp_pagenavi(array( 'query' => $query ));
				wp_reset_query(); 
			?>
		</section>
	</div>

<?php get_footer(); ?>