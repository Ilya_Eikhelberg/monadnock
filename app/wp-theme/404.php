<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

<?php
$args = array(
    'page_id' => 661,
    'posts_per_page' => 1
) ;

$query = new WP_Query( $args ); ?>
<?php if ( $query->have_posts() ) : ?>
    <?php while ( $query->have_posts() ) : $query->the_post();?>
        <?php get_template_part( 'flexible-content-rows'); ?>
    <?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>