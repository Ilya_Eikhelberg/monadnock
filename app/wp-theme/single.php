<?php 
    get_header();
   
?>
    

    <?php if ( have_posts() ) : ?>
        <?php while (have_posts() ) : the_post(); 
            $img = wp_get_attachment_image_src(get_post_thumbnail_id( $id ), 'full')[0];
            $prev_post = get_previous_post(); 
            $next_post = get_next_post(); 
        ?>
          
     
        <a href="<?php echo the_permalink( get_page_by_path( 'news' ) ); ?>" target="_self" class="single-nav-link single-nav-link_back">BACK TO ARTICLES</a>
   
        <?php echo get_the_content(); ?>

        <nav class="single-nav"> 
            <?php if(!empty( $prev_post )) : ?>

                <a href="<?php echo get_permalink( $prev_post->ID ); ?>" target="_self" class="pagination pagination_prev">PREVIOUS</a>

            <?php endif; ?>
            <?php if (!empty( $next_post )): ?> 
                <a href="<?php echo get_permalink( $next_post->ID ); ?>" target="_self" class="pagination pagination_next">NEXT</a>
            <?php endif; ?>
        </nav>

        <?php get_template_part( 'flexible-content-rows'); ?>

               
        <?php endwhile; ?>
    <?php endif; ?>

    <?php wp_reset_query(); ?>

<?php get_footer(); ?>