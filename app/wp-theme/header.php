<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">
    <title><?php bloginfo('name'); ?> <?php wp_title("|", true); ?></title> 
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->

   
    <!-- <script>
        (function(d) {
        var config = { 
          kitId: 'jnz6gjt',
          scriptTimeout: 3000,
          async: true
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
        })(document); 
    </script>

    <script>
        // Asynchronously load non-critical css 
        function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }
        // load css file async
        loadCSS("//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css");
    </script> -->

    <?php 
        wp_head();
        // $logo = get_field('site_logo', 'option')['url']; 
        // $linked_in = get_field('linked_in', 'options');
    ?> 
</head>   
 

<body <?php body_class(); ?> >
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Add your site or application content here -->

    <form action="<?php echo home_url( '/' ); ?>" class="form-search">
        <input type="search" value="<?php echo get_search_query() ?>" placeholder="Search by product name or keyword..." name="s">
    </form>

    <header class="header">
        <div class="search-trigger"></div>
        <div class="nav-trigger"></div>

        <a href="<?php echo get_settings('home'); ?>" class="site-logo header-logo">
            <img src="<?php echo $logo; ?>" alt="">
        </a>
        <nav class="primary-nav">
            <div class="primary-nav__group">
                <?php wp_nav_menu(
                    array( 'theme_location' => 'primary',
                        'container' => false,
                    ) );
                ?>

                <div class="subscription">
                     <?php 
                        // gravity_form_enqueue_scripts(2, true);
                        // gravity_form(2, true, false, false, '', true, 1); 
                     ?>  
                </div>
                
                <ul class="nav-social-list">
                    <li><a href="" target="_blank" class="linked-in"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </nav>
        
    </header>