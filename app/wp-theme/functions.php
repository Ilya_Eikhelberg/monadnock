<?php
function setup() {
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => 'Header',
        'social'  => 'Footer',
    ) );
    // See: https://codex.wordpress.org/Function_Reference/add_theme_support
    add_theme_support('post-thumbnails');
    add_image_size( 'full_hd', 1920, 0, false );

    if (function_exists('acf_add_options_page')) {
        acf_add_options_page();
    }
}

add_action( 'after_setup_theme', 'setup' );
// custom excerpt
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
function _excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', '_excerpt_length', 999 );


add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
    $content = ' }, false );';
    return $content;
}

add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
/**
 * Changes the default Gravity Forms AJAX spinner.
 */
function tgm_io_custom_gforms_spinner( $src ) {
    return get_stylesheet_directory_uri() . '/images/ajax-loader.gif';
    
}


/**
 * Enqueues scripts and styles for front end.
 */
function add_async_attribute($tag, $handle) {
    if ( 'minified-js' !== $handle )
        return $tag;
    return str_replace( ' src', ' async="async" src', $tag );
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function scripts_styles() { 
    wp_enqueue_script(
        'manifest-js',
        get_template_directory_uri() . '/js/manifest.js',
        array(), 
        filemtime(get_template_directory() . '/js/manifest.js'),
        true
    );
    wp_enqueue_script(
        'vendor-js',
        get_template_directory_uri() . '/js/vendor.js',
        array(), 
        filemtime(get_template_directory() . '/js/vendor.js'),
        true
    );
    wp_enqueue_script(
        'app-js',
        get_template_directory_uri() . '/js/app.js',
        array(), 
        filemtime(get_template_directory() . '/js/app.js'),
        true
    );
    wp_enqueue_style(
        'styles',
        get_template_directory_uri() . '/css/style.css',
        array(),
        filemtime(get_template_directory() . '/css/style.css'),
        false
    );
}
add_action( 'wp_enqueue_scripts', 'scripts_styles' );

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
// function form_submit_button( $button, $form ) {
//     return "<button class='btn' id='gform_submit_button_{$form['id']}'></button>";
// }

add_filter('gform_init_scripts_footer', '__return_true');