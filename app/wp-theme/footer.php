<?php
/**
 * The template for displaying the footer.
 */

// $copy = get_field('copyright', 'option');

?>
     

        <footer class="footer">
        	<div class="footer__group">
    			<span>&copy; <?php the_date('Y'); ?> </span>
        	</div> 
        	
        </footer> 
        
        <!--[if IE 8]>
        <script>window.isMsIe = 8;</script><![endif]-->
        <!--[if lte IE 7]>
        <script>window.isMsIe = 7;</script><![endif]-->
        <?php wp_footer(); ?>
        
    </body>
</html>

