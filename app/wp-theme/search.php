<?php 

	get_header();

	// $image = get_field('background_search', 'option')['url'];
	$s = get_search_query();
	$args = array(
        's' => $s
    );
   	$query = new WP_Query($args); 
 ?>




<?php if($query->have_posts()) : ?>
	<section class="">

		<?php while($query->have_posts()) : $query->the_post(); 
			$header = get_the_title();
			$content = get_the_excerpt();
			$url = get_permalink();
		?>
		
		   
            <h2 class=""><?php echo $header; ?></h2>
            <p><?php echo $content; ?>
            <a href="<?php echo $url; ?>" class="btn">READ MORE</a>

		<?php endwhile; ?>

	</section>
<?php endif; ?>
 <?php get_footer(); ?>