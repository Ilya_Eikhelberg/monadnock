export var navIsOpen = false;
export function navigationEvents() {
	$(document).on('click', '.menu-btn', function (e) {
		toggle();
	}).on('keyup', function (e) {
		if (e.keyCode == 27 && navIsOpen) {
			toggle();
		}
	});
}
function toggle() {
	navIsOpen = !navIsOpen ? true : false;
	$('body').toggleClass('js-nav-open');
}