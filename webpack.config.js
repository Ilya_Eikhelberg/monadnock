var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var	production = require('./config.json').production;

module.exports = {
	devtool: !production ? 'cheap-eval-source-map' : 'source-map',
	watch: false,
	entry: {
		app: './app/js/index.js',
		// bundle2: './app/js/index2.js'
	},
	output: {
		path: path.resolve(__dirname, "dist/js"),
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: [	{ loader: 'babel-loader' }, 
						{ loader: 'jshint-loader', options: {
								camelcase: true,
								esversion: 6,
								emitErrors: false,
								failOnHint: false
							}
						}
					]
			}, { 
				test: /\.css$/, use: ExtractTextPlugin.extract({ use: 'css-loader'}) 
			}
		] 
	},
	resolve: {
		extensions: ['.js'],
		modules: [path.resolve(__dirname, "app/js"), "node_modules"],
		alias: {
			waypoints: path.resolve(__dirname, "node_modules/waypoints/lib/noframework.waypoints.js")
		}
	},
	devServer: {
		contentBase: [path.join(__dirname, "./app/html"), path.join(__dirname, "dist")],
		compress: true,
		watchContentBase: true,
		// https: true,
		// port: 9000
	},
	plugins: [
		new ExtractTextPlugin('imported.css'),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor', // Specify the common bundle's name.
			minChunks: function (module) {
			   // this assumes your vendor imports exist in the node_modules directory
			   return module.context && module.context.indexOf('node_modules') !== -1;
			}
		}),
		//CommonChunksPlugin will now extract all the common modules from vendor and main bundles
		new webpack.optimize.CommonsChunkPlugin({
			name: 'manifest' //But since there are no more common modules between them we end up with just the runtime code included in the manifest file
		}),
		new webpack.optimize.UglifyJsPlugin({
			beautify: !production,
			compress: {
				warnings: true
			},
			sourceMap: true,
			comments: !production,
			minimize: production
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		})
	]
}